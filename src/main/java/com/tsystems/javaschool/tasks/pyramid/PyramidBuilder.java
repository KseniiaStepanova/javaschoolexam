package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int[][] result;
        Queue<Integer> numbers = new PriorityQueue<>();

        if (inputNumbers.size() < Integer.MAX_VALUE - 1 && !inputNumbers.contains(null)) {

            inputNumbers.sort(Comparator.comparing(Integer::valueOf));
            numbers.addAll(inputNumbers);

            int counter = 0;
            int rows = 1;
            int columns = 1;

            while (counter < numbers.size()) {
                counter = counter + rows;
                rows++;
                columns = columns + 2;
            }

            rows = rows - 1;
            columns = columns - 2;

            result = new int[rows][columns];

            if (numbers.size() == counter) {

                int center = (columns / 2);
                int count = 1;
                int index = 0;

                for (int i = 0, offset = 0; i < rows; i++, offset++, count++) {
                    int start = center - offset;
                    for (int j = 0; j < count * 2; j += 2, index++) {
                        result[i][start + j] = numbers.poll();
                    }
                }
            } else {
                throw new CannotBuildPyramidException();
            }

        } else {
            throw new CannotBuildPyramidException();
        }
        return result;
    }
}
