package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Double.valueOf;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String result = null;
        if (statement != null) {
            if (isBalancedBrackets(statement) & isNotDuplicatedOperators(statement) & isValidSeparator(statement)) {
                String calcResult = doMath(statement);
                if (calcResult != null)
                result = calcResult.replace(".0", "");
            }
        }
        return result;
    }

    public static boolean isBalancedBrackets(String statement) {
        Stack<Character> st = new Stack<>();
        boolean result = false;
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == ')') {
                if (!st.empty()) {
                    st.pop();
                } else break;
            }
            if (statement.charAt(i) == '(') {
                st.add(statement.charAt(i));
            }
            result = st.empty();
        }
        return result;
    }

    public static boolean isNotDuplicatedOperators(String statement) {
        Pattern pattern = Pattern.compile("[\\-\\+\\*/.]{2,}");
        Matcher matcher = pattern.matcher(statement);
        boolean result = true;
        while (matcher.find()) {
            result = false;
        }
        return result;
    }

    public static boolean isValidSeparator(String statement) {
        Pattern pattern = Pattern.compile("[,]");
        Matcher matcher = pattern.matcher(statement);
        boolean result = true;
        while (matcher.find()) {
            result = false;
        }
        return result;
    }

    public static String doMath(String statement) {
        Pattern pattern = Pattern.compile("\\(.+\\)");
        Matcher matcher = pattern.matcher(statement);
        String insideBrackets = null;
        String statementNoBrackets = statement;

        while (matcher.find()) {
            insideBrackets = matcher.group();
            String inside = String.valueOf(calculate(insideBrackets.replace("(", "")
                    .replace(")", "")));
            statementNoBrackets = statement.replace(insideBrackets, inside);
        }
        return calculate(statementNoBrackets);
    }

    public static String  calculate(String statement) {
        Stack<String> initialStatement = new Stack<>();
        Pattern pattern = Pattern.compile("([0-9.]+)|([\\-\\+\\*/]{1})");
        String result = null;

        Matcher matcher = pattern.matcher(statement);
        while (matcher.find()) {
            initialStatement.addElement(matcher.group());
        }

        Stack<String> multAndDiv = new Stack<>();
        Stack<String> subAndAdd = new Stack<>();

        outer:
        {
            // calculate multiplication and division
            for (int i = 0; i < initialStatement.size(); i++) {
                if (initialStatement.get(i).matches("([0-9.])+") | initialStatement.get(i).equals("+") | initialStatement.get(i).equals("-")) {
                    multAndDiv.push(initialStatement.get(i));
                }
                if (initialStatement.get(i).equals("*")) {
                    if (initialStatement.get(i + 1).matches("([0-9.])+")) {
                        result = String.valueOf(valueOf(multAndDiv.pop()) * valueOf(initialStatement.get(i + 1)));
                    } else {
                        result = String.valueOf(valueOf(multAndDiv.pop()) * valueOf(initialStatement.get(i + 1) + initialStatement.get(i + 2)));
                        i++;
                    }
                    multAndDiv.push(result);
                    i++;
                }
                if (initialStatement.get(i).equals("/")) {
                    if (initialStatement.get(i + 1).matches("([0-9.])+")) {
                        if (valueOf(initialStatement.get(i + 1)) == 0.0) {
                            break outer;
                        }
                        result = String.valueOf(valueOf(multAndDiv.pop()) / valueOf(initialStatement.get(i + 1)));
                    } else {
                        result = String.valueOf(valueOf(multAndDiv.pop()) / valueOf(initialStatement.get(i + 1) + initialStatement.get(i + 2)));
                        i++;
                    }
                    multAndDiv.push(result);
                    i++;
                }
            }

            // calculate addition and subtraction
            for (int i = 0; i < multAndDiv.size(); i++) {
                if (multAndDiv.get(i).matches("([0-9.])+")) {
                    subAndAdd.push(multAndDiv.get(i));
                }
                if (multAndDiv.get(i).equals("+")) {
                    if (initialStatement.get(i + 1).matches("([0-9.])+")) {
                        result = String.valueOf(valueOf(subAndAdd.pop()) + valueOf(multAndDiv.get(i + 1)));
                    } else {
                        result = String.valueOf(valueOf(subAndAdd.pop()) + valueOf(multAndDiv.get(i + 1) + initialStatement.get(i + 2)));
                        i++;
                    }
                    subAndAdd.push(result);
                    i++;
                }
                if (multAndDiv.get(i).equals("-")) {
                    if (initialStatement.get(i + 1).matches("([0-9.])+")) {
                        result = String.valueOf(valueOf(subAndAdd.pop()) - valueOf(multAndDiv.get(i + 1)));
                    } else {
                        result = String.valueOf(valueOf(subAndAdd.pop()) - valueOf(multAndDiv.get(i + 1) + initialStatement.get(i + 2)));
                        i++;
                    }
                    subAndAdd.push(result);
                    i++;
                }
            }
        }
        return result;
    }
}